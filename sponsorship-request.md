# Diversity and Inclusion Event Sponsorship Request


Note: If you are applying this template to an existing issue, please follow these steps:
1. Copy text from existing issue.
2. Apply 'sponsorship-request' template to issue.
3. Paste text from original issue into the appropriate field at the bottom of this template.
4. Update remaining fields.

<!-- 
### Steps for Team Member requesting sponsorship -->

<!-- - [x] Confirm the issue is confidential
- [x] Confirm the issue is assign to lmcnally1 and cwilliams3 -->
<!-- - [:white_check_mark:] Confirm the issue has the _DIB Sponsorship_ label -->

## Event Info

Please include the following information about your event:

- Group Name: Society for Promotion of Electronics Culture (SPEC), National Institute of Technology Hamirpur, India
- Group URL: https://specnith.com/
- Event Name: Electrothon
- Event URL: https://electrothon.specnith.com/
- Organizer Name: Yuvraj Kadale
- Location: NIT Hamirpur, Hamirpur (H.P.), India
- Date: February 19th and 20th, 2022
- Time: 48-Hour Hackathon
- Will there be a speaker from the wider GitLab community or a talk about GitLab? - You are welcome to provide us with a speaker for an informative session during the event.
- Number of attendees: 1000
- Audience profile (ex: developers, executives, students, non-technical, etc.): Student Developers
- Prospectus: https://drive.google.com/file/d/1zyRhic0lWuBUTy8lc7-RfxuVd01CT01m/view

## Event Description

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->
### Overview
We are organizing a student-run National Level hacking event “Electrothon 4.0 - What leads and propels the world are not machines, but ideas”. Electrothon aims to build an environment where people from different backgrounds come together to discover wild ideas, share experiences, fuel their works, and celebrate technology.
It is a platform where exploring minds come as individuals but leave as a community. It is open to developers, hobbyists, and tech enthusiasts from our institute as well as other colleges. The previous iteration - Electrothon'21, witnessed 2200+ registration nationwide. The 400+ shortlisted participants created 66 Github Repositories with 1500+ commits and 2000000+ lines of code. The support of 35+ sponsors worked as a backbone for us to accomplish 48 hours of hacking to deliver 100+ innovations.

### Tracks and Domains
The hackathon has five categories -
1. Hunger Eradication
2. Cyber Bullying
3. Pandemics
4. Climate Change
5. Natural Disaster

### Details
People can participate in a team varying from 2 to 4 members. The participants will have to propose exciting and innovative ideas in any of the above-mentioned categories. They can devise solutions based on hardware, software, or on both  Technology does not matter as long as it is manifested into a purpose. The ideas will go through an evaluation process followed by a shortlisting process. The participants will be free to choose the mechanism of their preference and can develop a hardware-based or software-based or both, projects. Furthermore, the selected teams will be invited to an incubation room to develop a prototype of their project.
The event will last for two days. On the final day, the teams will have to present their creation. The final evaluation will take place in the form of a presentation, which will be judged by eminent Tech Speakers and Judges from around the world.
During the evaluation process, all the products/projects will be uploaded to GitHub, for participants to collaborate further after the culmination of the event.

## Our Commitment to Diversity, Inclusion, and Belongingness

Taking into consideration that GitLab lays special emphasis on promoting diversity, inclusion and belongingness, we believe that our event Electrothon meets all your requirements. We here at Electrothon give special emphasis on encouraging less fortunate students, women, and minorities to push the barricades aside and step forward towards a brighter future. You can refer to our code of conduct for the same.

Please go through our Code of Conduct here: https://hackcodeofconduct.org/2012-electrothon

Our team works hard to provide the participants with all the resources they will possibly require during the hackathon, so as to eliminate the risk of bias. This Electrothon, we will focus on being a platform for culturally, ethnically, racially, and sexually diverse talented people to showcase their talents and build ideas in significant sectors such as Financial Technology, AR/VR Blockchain and many more.

Our participants are student developers and enthusiasts working in the field of Virtual Continuum, AI/ML, Cryptography. They use DevOp, Cloud-based services, JS frameworks, AR/VR platforms, IoT services, etc.

One of our prime motives is to create a lucrative market for the exchange of knowledge. We focus not only on benefitting the participants but also to broaden the minds of our audience. To achieve the same objectives we have planned a Q&A session for the audience where they will be able to interact with the developers. Electrothon will also witness a round-the-table discussion for the participants, therefore providing them an opportunity to ponder upon topics, we usually don't deem worthy of reflection such as the increasing dependence on technology.

We here take special care to cater to the needs of our sponsors and hence we will conduct an informative session about open source and GitLab. You are also welcome to interact with the developers to provide them an insight into your platform.

## Type of request

Check all that apply

- [:white_check_mark:] Speaker
- [:white_check_mark:] Sponsorship
- [:white_check_mark:] Swag
